public class Motocicleta extends Vehiculo implements PuedeCircular{

	private double potenciaMotor;
	private boolean tieneMaletero;

	/* constructors */

	public Motocicleta(){
		super();
		this.potenciaMotor = 0.0;
		this.tieneMaletero = false;
	}

	public Motocicleta(String marca, String modelo, String color, double kilometros, int numPuertas, int numPlazas, double potenciaMotor, boolean tieneMaletero){
		super(marca, modelo, color, kilometros, numPuertas, numPlazas);
		this.setPotenciaMotor(potenciaMotor);
		this.setTieneMaletero(tieneMaletero);
	}

	/* getters */

	public double getPotenciaMotor(){
		return potenciaMotor;
	}

	public String getTieneMaletero(){
		if(tieneMaletero == true){
			return "Sí";
		}
		else{
			return "No";
		}
	}

	/* setters */

	public void setPotenciaMotor(double potenciaMotor){
		this.potenciaMotor = potenciaMotor;
	}

	public void setTieneMaletero(boolean tieneMaletero){
		this.tieneMaletero = tieneMaletero;
	}

	/* mètodes */

	public String brincar(){
		return "La motocicleta está brincando";
	}

	public String aparcar(){
		return "La motocicleta está aparcando";
	}

	@Override
	public String arrancar(){
		return "La motocicleta está arrancando";
	}

	@Override
	public String acelerar(){
		return "La motocicleta está acelerando";
	}

	@Override
	public String frenar(){
		return "La motocicleta está frenando";
	}

	@Override
	public String circular(){
		return "Esto es una motocicleta y las motocicletas pueden circular por carreteras, autovías y autopistas";
	}

	@Override
	public String toString(){
		return super.toString()
				+ "\nPotencia del motor: " + getPotenciaMotor()
				+ "\nTiene maletero: " + getTieneMaletero();
	}
}