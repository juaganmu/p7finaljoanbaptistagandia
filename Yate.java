public class Yate extends Vehiculo implements PuedeNavegar{

	private boolean tieneCocina;
	private int numMotores;
	private double metrosEslora;

	/* constructors */

	public Yate(){
		super();
		this.tieneCocina = false;
		this.numMotores = 0;
		this.metrosEslora = 0.0;
	}

	public Yate(String marca, String modelo, String color, double kilometros, int numPuertas, int numPlazas, boolean tieneCocina, int numMotores, double metrosEslora){
		super(marca, modelo, color, kilometros, numPuertas, numPlazas);
		this.setTieneCocina(tieneCocina);
		this.setNumMotores(numMotores);
		this.setMetrosEslora(metrosEslora);
	}

	/* getters */

	public String getTieneCocina(){
		if(tieneCocina == true){
			return "Sí";
		}
		else{
			return "No";
		}
	}

	public int getNumMotores(){
		return numMotores;
	}

	public double getMetrosEslora(){
		return metrosEslora;
	}

	/* setters */

	public void setTieneCocina(boolean tieneCocina){
		this.tieneCocina = tieneCocina;
	}

	public void setNumMotores(int numMotores){
		this.numMotores = numMotores;
	}

	public void setMetrosEslora(double metrosEslora){
		this.metrosEslora = metrosEslora;
	}

	/* mètodes */

	public String zarpar(){
		return "El yate está zarpando";
	}

	public String atracar(){
		return "El yate está atracando";
	}

	@Override
	public String arrancar(){
		return "El yate está arrancando";
	}

	@Override
	public String acelerar(){
		return "El yate está acelerando";
	}

	@Override
	public String frenar(){
		return "El yate está frenando";
	}

	@Override
	public String navegar(){
		return "Esto es un yate y los yates pueden circular por los mares y dentro de los puertos";
	}

	@Override
	public String toString(){
		return super.toString()
				+ "\nTiene cocina: " + getTieneCocina()
				+ "\nNº de motores: " + getNumMotores()
				+ "\nMetros de eslora: " + getMetrosEslora();
	}
}