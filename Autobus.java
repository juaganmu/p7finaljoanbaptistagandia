public class Autobus extends Vehiculo implements PuedeCircular{

	private String tipoRecorrido;
	private boolean esEscolar;

	/* constructors */

	public Autobus(){
		super();
		this.tipoRecorrido = "Urbano";
		this.esEscolar = false;
	}

	public Autobus(String marca, String modelo, String color, double kilometros, int numPuertas, int numPlazas, String tipoRecorrido, boolean esEscolar){
		super(marca, modelo, color, kilometros, numPuertas, numPlazas);
		this.setTipoRecorrido(tipoRecorrido);
		this.setEsEscolar(esEscolar);
	}

	/* getters */

	public String getTipoRecorrido(){
		return tipoRecorrido;
	}

	public String getEsEscolar(){
		if(esEscolar == true){
			return "Sí";
		}
		else{
			return "No";
		}
	}

	/* setters */

	public void setTipoRecorrido(String tipoRecorrido){
		this.tipoRecorrido = tipoRecorrido;
	}

	public void setEsEscolar(boolean esEscolar){
		this.esEscolar = esEscolar;
	}

	/* mètodes */

	public String abrirPuertas(){
		return "El autobús está abriendo las puertas";
	}

	public String aparcar(){
		return "El autobús está aparcando";
	}

	@Override
	public String arrancar(){
		return "El autobús está arrancando";
	}

	@Override
	public String acelerar(){
		return "El autobús está acelerando";
	}

	@Override
	public String frenar(){
		return "El autobús está frenando";
	}

	@Override
	public String circular(){
		return "Esto es un autobús y los autobuses pueden circular por carreteras, autovías y autopistas";
	}

	@Override
	public String toString(){
		return super.toString()
				+ "\nTipo de recorrido: " + getTipoRecorrido()
				+ "\nAutobus escolar: " + getEsEscolar();
	}
}