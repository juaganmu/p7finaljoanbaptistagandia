public class Coche extends Vehiculo implements PuedeCircular{

	private int numAirbags;
	private boolean techoSolar;
	private boolean tieneRadio;

	/* constructors */

	public Coche(){
		super();
		this.numAirbags = 0;
		this.techoSolar = false;
		this.tieneRadio = false;
	}

	public Coche(String marca, String modelo, String color, double kilometros, int numPuertas, int numPlazas, int numAirbags, boolean techoSolar, boolean tieneRadio){
		super(marca, modelo, color, kilometros, numPuertas, numPlazas);
		this.setNumAirbags(numAirbags);
		this.setTechoSolar(techoSolar);
		this.setTieneRadio(tieneRadio);
	}

	/* getters */

	public int getNumAirbags(){
		return numAirbags;
	}

	public String getTechoSolar(){
		if(techoSolar == true){
			return "Sí";
		}
		else{
			return "No";
		}
	}

	public String getTieneRadio(){
		if(tieneRadio == true){
			return "Sí";
		}
		else{
			return "No";
		}
	}

	/* setters */

	public void setNumAirbags(int numAirbags){
		if(numAirbags >= 0){
			this.numAirbags = numAirbags;
		}
	}

	public void setTechoSolar(boolean techoSolar){
		this.techoSolar = techoSolar;
	}

	public void setTieneRadio(boolean tieneRadio){
		this.tieneRadio = tieneRadio;
	}

	/* mètodes */

	public String tunear(String color){
		setKilometros(0.0);
		if (getTechoSolar() == "Sí"){
			setTechoSolar(true);
			return "Cuentakilómetros a 0. Techo solar instalado. Pintado de color: " + color;
		}
		setColor(color);
		return "Cuentakilómetros a 0. Pintado de color: " + color;
	}

	public String aparcar(){
		return "El coche está aparcando";
	}

	@Override
	public String arrancar(){
		return "El coche está arrancando";
	}

	@Override
	public String acelerar(){
		return "El coche está acelerando";
	}

	@Override
	public String frenar(){
		return "El coche está frenando";
	}

	@Override
	public String circular(){
		return "Esto es un coche y los coches pueden circular por carreteras, autovías y autopistas";
	}

	@Override
	public String toString(){
		return super.toString()
				+ "\nNº de airbags: " + getNumAirbags()
				+ "\nTecho Solar: " + getTechoSolar()
				+ "\nTiene Radio: " + getTieneRadio();
	}
}