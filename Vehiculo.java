public abstract class Vehiculo{

	private String matricula;
	private String marca;
	private String modelo;
	private String color;
	private double kilometros;
	private int numPuertas;
	private int numPlazas;
	private static int numVehiculos = 0;
	public static final int MAX_VEHICULOS = 5;

	/* Constructors */

	public Vehiculo(){
		this.setMatricula();
		this.marca = "";
		this.modelo = "";
		this.color = "blanco";
		this.kilometros = 0.0;
		this.numPuertas = 0;
		this.numPlazas = 0;
		numVehiculos++;
	}

	public Vehiculo(String marca, String modelo, String color, double kilometros, int numPuertas, int numPlazas){
		this();
		this.setMarca(marca);
		this.setModelo(modelo);
		this.setColor(color);
		this.setKilometros(kilometros);
		this.setNumPuertas(numPuertas);
		this.setNumPlazas(numPlazas);
	}

	/* getters */
	public static int getNumVehiculos(){
		return numVehiculos;
	}

	public String getMatricula(){
		return matricula;
	}

	public String getMarca(){
		return marca;
	}

	public String getModelo(){
		return modelo;
	}

	public String getColor(){
		return color;
	}

	public double getKilometros(){
		return kilometros;
	}

	public int getNumPuertas(){
		return numPuertas;
	}

	public int getNumPlazas(){
		return numPlazas;
	}

	/* setters */

	public void setMatricula(){
		int midaMatricula = 7;
		String listCaract = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		char[] listCaract_char = listCaract.toCharArray();
		char[] matriculaChar  = new char[midaMatricula];

		for(int i= 0; i < midaMatricula; i++){
			matriculaChar[i] = listCaract_char[(int)((Math.random()*36))];
		}

		matricula = String.valueOf(matriculaChar);
	}

	public void setMarca(String marca){
		this.marca = marca;
	}

	public void setModelo(String modelo){
		this.modelo = modelo;
	}

	public void setColor(String color){
		this.color = color;
	}

	public void setKilometros(double kilometros){
		if (kilometros > 0.0){
			this.kilometros = kilometros;
		}
	}

	public void setNumPuertas(int numPuertas){
		if(numPuertas > 0){
			this.numPuertas = numPuertas;
		}
	}

	public void setNumPlazas(int numPlazas){
		if(numPlazas > 0){
			this.numPlazas = numPlazas;
		}
	}

	/* mètodes abastractes */

	public abstract String arrancar();
	public abstract String acelerar();
	public abstract String frenar();

	/* mètodes no abstractes */

	@Override
	public String toString(){
		return "\nMatrícula: " + getMatricula()
				+ "\nMarca: " + getMarca()
				+ "\nModelo: " + getModelo()
				+ "\nColor: " + getColor()
				+ "\nKilometros: " + getKilometros()
				+ "\nNº de puertas: " + getNumPuertas()
				+ "\nNº de plazas: " + getNumPlazas();
	}

}