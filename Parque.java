import java.util.ArrayList;
import javax.swing.JOptionPane;

public class Parque{

	public static void main(String[] args){

		int selected = 0;
		int selectedOpciones = 0;
		boolean exit = false;
		boolean exitOpciones = false;

		Parque parque = new Parque();
		ArrayList<Vehiculo> miArray = new ArrayList<Vehiculo>();

		
		do{
			String txtMenuPrincipal;
			txtMenuPrincipal = "Parque móvil de Joan Baptista Gandia. Selecciona una de las siguientes opciones:\n"
						+ "1. Crear un coche (con datos)\n"
						+ "2. Crear un coche (sin datos)\n"
						+ "3. Crear un autobús (con datos)\n"
						+ "4. Crear un autobús (sin datos)\n"
						+ "5. Crear una motocicleta (con datos)\n"
						+ "6. Crear una motocicleta (sin datos)\n"
						+ "7. Crear una avioneta (con datos)\n"
						+ "8. Crear una avioneta (sin datos)\n"
						+ "9. Crear un yate (con datos)\n"
						+ "10. Crear un yate (sin datos)\n"
						+ "11. Mostrar características de todos los vehículos\n"
						+ "12. Más operaciones\n"
						+ "13. Salir del programa\n";

			try{
				txtMenuPrincipal = JOptionPane.showInputDialog(null, txtMenuPrincipal, "Entrada", JOptionPane.QUESTION_MESSAGE);
				selected = Integer.parseInt(txtMenuPrincipal);
			}
			catch(Exception e){	
				JOptionPane.showMessageDialog(null, "La opción elegida ha de ser un número entero de los indicados en el menú.");
			}

			switch(selected){
				case 1:
				{
					boolean auxTechoSolar;
					boolean auxTieneRadio; 

					if(Vehiculo.getNumVehiculos() >= Vehiculo.MAX_VEHICULOS){
						JOptionPane.showMessageDialog(null, "No es posible crear más vehículos\nNo caben en el parque.");
						break;
					}

					String marca = JOptionPane.showInputDialog(null, "Introduce la marca del coche: ");
					String modelo = JOptionPane.showInputDialog(null, "Introduce el modelo del coche: ");
					String color = JOptionPane.showInputDialog(null, "Introduce el color del coche: ");
					double kilometros = Double.parseDouble(JOptionPane.showInputDialog(null, "Introduce los kilómetros que tiene el coche: "));
					int numPuertas = Integer.parseInt(JOptionPane.showInputDialog(null, "Introduce el número de puertas del coche: "));
					int numPlazas = Integer.parseInt(JOptionPane.showInputDialog(null, "Introduce el número de plazas del coche: "));
					int numAirbags = Integer.parseInt(JOptionPane.showInputDialog(null, "Introduce el número de airbags del coche: "));
					int techoSolar = JOptionPane.showOptionDialog(null,"¿Tiene techo solar?", "Pregunta", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null,null,null);
					int tieneRadio = JOptionPane.showOptionDialog(null,"¿Tiene radio?", "Pregunta", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null,null,null);

					if(techoSolar == 1){
						auxTechoSolar = false;
					}
					else{
						auxTechoSolar = true;
					}

					if(tieneRadio == 1){
						auxTieneRadio = false;
					}
					else{
						auxTieneRadio = true;
					}

					miArray.add(new Coche(marca, modelo, color, kilometros, numPuertas, numPlazas, numAirbags, auxTechoSolar, auxTieneRadio));
					JOptionPane.showMessageDialog(null, "Los datos del coche creado són\n" + miArray.get(miArray.size()-1));
					break;

				}
				
				case 2:
				{
					if(Vehiculo.getNumVehiculos() >= Vehiculo.MAX_VEHICULOS){
						JOptionPane.showMessageDialog(null, "No es posible crear más vehículos\nNo caben en el parque.");
						break;
					}

					miArray.add(new Coche());
					JOptionPane.showMessageDialog(null, "Los datos del coche creado són\n" + miArray.get(miArray.size()-1));
					break;
				}

				case 3:
				{
					boolean auxEsEscolar;

					if(Vehiculo.getNumVehiculos() >= Vehiculo.MAX_VEHICULOS){
						JOptionPane.showMessageDialog(null, "No es posible crear más vehículos\nNo caben en el parque.");
						break;
					}

					String marca = JOptionPane.showInputDialog(null, "Introduce la marca del bus: ");
					String modelo = JOptionPane.showInputDialog(null, "Introduce el modelo del bus: ");
					String color = JOptionPane.showInputDialog(null, "Introduce el color del bus: ");
					double kilometros = Double.parseDouble(JOptionPane.showInputDialog(null, "Introduce los kilómetros que tiene el bus: "));
					int numPuertas = Integer.parseInt(JOptionPane.showInputDialog(null, "Introduce el número de puertas del bus: "));
					int numPlazas = Integer.parseInt(JOptionPane.showInputDialog(null, "Introduce el número de plazas del bus: "));
					String tipoRecorrido = JOptionPane.showInputDialog(null, "Introduce el tipo de recorrido: ");
					int esEscolar = JOptionPane.showOptionDialog(null,"¿Es un bus escolar?", "Pregunta", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null,null,null);

					if(esEscolar == 1){
						auxEsEscolar = false;
					}
					else{
						auxEsEscolar = true;
					}

					miArray.add(new Autobus(marca, modelo, color, kilometros, numPuertas, numPlazas, tipoRecorrido, auxEsEscolar));
					JOptionPane.showMessageDialog(null, "Los datos del bus creado són\n" + miArray.get(miArray.size()-1));
					break;
				}

				case 4:
				{
					if(Vehiculo.getNumVehiculos() >= Vehiculo.MAX_VEHICULOS){
						JOptionPane.showMessageDialog(null, "No es posible crear más vehículos\nNo caben en el parque.");
						break;
					}

					miArray.add(new Autobus());
					JOptionPane.showMessageDialog(null, "Los datos del autobus creado són\n" + miArray.get(miArray.size()-1));
					break;
				}

				case 5:
				{
					boolean auxTieneMaletero;

					if(Vehiculo.getNumVehiculos() >= Vehiculo.MAX_VEHICULOS){
						JOptionPane.showMessageDialog(null, "No es posible crear más vehículos\nNo caben en el parque.");
						break;
					}

					String marca = JOptionPane.showInputDialog(null, "Introduce la marca de la motocicleta: ");
					String modelo = JOptionPane.showInputDialog(null, "Introduce el modelo de la motocicleta: ");
					String color = JOptionPane.showInputDialog(null, "Introduce el color de la motocicleta: ");
					double kilometros = Double.parseDouble(JOptionPane.showInputDialog(null, "Introduce los kilómetros que tiene la motocicleta: "));
					int numPuertas = Integer.parseInt(JOptionPane.showInputDialog(null, "Introduce el número de puertas de la motocicleta: "));
					int numPlazas = Integer.parseInt(JOptionPane.showInputDialog(null, "Introduce el número de plazas de la motocicleta: "));
					double potenciaMotor = Double.parseDouble(JOptionPane.showInputDialog(null, "Introduce la potencia del motor de la motocicleta: "));
					int tieneMaletero = JOptionPane.showOptionDialog(null,"¿Tiene maletero?", "Pregunta", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null,null,null);


					if(tieneMaletero == 1){
						auxTieneMaletero = false;
					}
					else{
						auxTieneMaletero = true;
					}

					miArray.add(new Motocicleta(marca, modelo, color, kilometros, numPuertas, numPlazas, potenciaMotor, auxTieneMaletero));
					JOptionPane.showMessageDialog(null, "Los datos de la motocicleta creada són\n" + miArray.get(miArray.size()-1));
					break;
				}

				case 6:
				{
					if(Vehiculo.getNumVehiculos() >= Vehiculo.MAX_VEHICULOS){
						JOptionPane.showMessageDialog(null, "No es posible crear más vehículos\nNo caben en el parque.");
						break;
					}

					miArray.add(new Motocicleta());
					JOptionPane.showMessageDialog(null, "Los datos de la motocicleta creada són\n" + miArray.get(miArray.size()-1));
					break;
				}

				case 7:
				{
					if(Vehiculo.getNumVehiculos() >= Vehiculo.MAX_VEHICULOS){
						JOptionPane.showMessageDialog(null, "No es posible crear más vehículos\nNo caben en el parque.");
						break;
					}

					String marca = JOptionPane.showInputDialog(null, "Introduce la marca de la avioneta: ");
					String modelo = JOptionPane.showInputDialog(null, "Introduce el modelo de la avioneta: ");
					String color = JOptionPane.showInputDialog(null, "Introduce el color de la avioneta: ");
					double kilometros = Double.parseDouble(JOptionPane.showInputDialog(null, "Introduce los kilómetros que tiene la avioneta: "));
					int numPuertas = Integer.parseInt(JOptionPane.showInputDialog(null, "Introduce el número de puertas de la avioneta: "));
					int numPlazas = Integer.parseInt(JOptionPane.showInputDialog(null, "Introduce el número de plazas de la avioneta: "));
					String aeropuerto = JOptionPane.showInputDialog(null, "Introduce el nombre del aeropuerto donde tiene la base: ");
					int maxKg = Integer.parseInt(JOptionPane.showInputDialog(null, "Introduce el peso máx. en Kg. que puede cargar: "));

					miArray.add(new Avioneta(marca, modelo, color, kilometros, numPuertas, numPlazas, aeropuerto, maxKg));
					JOptionPane.showMessageDialog(null, "Los datos de la avioneta creada són\n" + miArray.get(miArray.size()-1));
					break;
				}

				case 8:
				{
					if(Vehiculo.getNumVehiculos() >= Vehiculo.MAX_VEHICULOS){
						JOptionPane.showMessageDialog(null, "No es posible crear más vehículos\nNo caben en el parque.");
						break;
					}

					miArray.add(new Avioneta());
					JOptionPane.showMessageDialog(null, "Los datos de la avioneta creada són\n" + miArray.get(miArray.size()-1));
					break;
				}

				case 9:
				{
					boolean auxTieneCocina;

					if(Vehiculo.getNumVehiculos() >= Vehiculo.MAX_VEHICULOS){
						JOptionPane.showMessageDialog(null, "No es posible crear más vehículos\nNo caben en el parque.");
						break;
					}

					String marca = JOptionPane.showInputDialog(null, "Introduce la marca del yate: ");
					String modelo = JOptionPane.showInputDialog(null, "Introduce el modelo del yate: ");
					String color = JOptionPane.showInputDialog(null, "Introduce el color del yate: ");
					double kilometros = Double.parseDouble(JOptionPane.showInputDialog(null, "Introduce los kilómetros que tiene el yate: "));
					int numPuertas = Integer.parseInt(JOptionPane.showInputDialog(null, "Introduce el número de puertas del yate: "));
					int numPlazas = Integer.parseInt(JOptionPane.showInputDialog(null, "Introduce el número de plazas del yate: "));
					int tieneCocina = JOptionPane.showOptionDialog(null,"¿Tiene cocina?", "Pregunta", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null,null,null);
					int numMotores = Integer.parseInt(JOptionPane.showInputDialog(null, "Introduce el número de motores del yate: "));
					double metrosEslora = Double.parseDouble(JOptionPane.showInputDialog(null, "Introduce los metros de eslora: "));

					if(tieneCocina == 1){
						auxTieneCocina = false;
					}
					else{
						auxTieneCocina = true;
					}

					miArray.add(new Yate(marca, modelo, color, kilometros, numPuertas, numPlazas, auxTieneCocina, numMotores, metrosEslora));
					JOptionPane.showMessageDialog(null, "Los datos del Yate creado són\n" + miArray.get(miArray.size()-1));
					break;
				}

				case 10:
				{
					if(Vehiculo.getNumVehiculos() >= Vehiculo.MAX_VEHICULOS){
						JOptionPane.showMessageDialog(null, "No es posible crear más vehículos\nNo caben en el parque.");
						break;
					}

					miArray.add(new Yate());
					JOptionPane.showMessageDialog(null, "Los datos del yate creado són\n" + miArray.get(miArray.size()-1));
					break;
				}

				case 11:
				{
					parque.mostrarArray(miArray);
					break;
				}

				case 12:
				{
					Vehiculo vehiculoOpciones = null;
					
					do{
						String matricula = JOptionPane.showInputDialog(null, "Introduce la matrícula del vehículo: ");
						
						for(Vehiculo v: miArray){
							if(matricula.equals(v.getMatricula())){
								vehiculoOpciones = v;
								break;
							}
						}
					}while(vehiculoOpciones ==  null);

					do{

						String txtMenuOpciones;
						txtMenuOpciones = "Selecciona una de las siguientes opciones:\n"
									+ "1. Salir del menú opciones\n"
									+ "2. Arranca el vehículo\n"
									+ "3. Acelera el vehículo\n"
									+ "4. Frena el vehículo\n";

						switch(parque.tipoVehiculo(vehiculoOpciones)){
							case 1:
								txtMenuOpciones = txtMenuOpciones + "5. Tunea el coche\n" + "6. Aparca el coche";
								break;
							case 2:
								txtMenuOpciones = txtMenuOpciones + "5. Abre las puertas\n" + "6. Aparca el bus";
								break;
							case 3:
								txtMenuOpciones = txtMenuOpciones + "5. Brinca con la moto\n" + "6. Aparca la moto";
								break;
							case 4:
								txtMenuOpciones = txtMenuOpciones + "5. Despega\n" + "6. Aterriza";
								break;
							case 5:
								txtMenuOpciones = txtMenuOpciones + "5. Zarpa\n" + "6. Atraca";
								break;
							default:
								JOptionPane.showMessageDialog(null, "Introduce un número del 1 al 6");
								break;
						}

						try{
							txtMenuOpciones = JOptionPane.showInputDialog(null, txtMenuOpciones, "Entrada", JOptionPane.QUESTION_MESSAGE);
							selectedOpciones = Integer.parseInt(txtMenuOpciones);
						}
						catch(Exception e){	
							JOptionPane.showMessageDialog(null, "La opción elegida ha de ser un número entero de los indicados en el menú.");
						}

						switch(selectedOpciones){
							case 1:
							{
								exitOpciones = true;
								break;
							}
							case 2:
							{
								JOptionPane.showMessageDialog(null, vehiculoOpciones.arrancar());
								break;
							}
							case 3:
							{
								JOptionPane.showMessageDialog(null, vehiculoOpciones.acelerar());
								break;
							}
							case 4:
							{
								JOptionPane.showMessageDialog(null, vehiculoOpciones.frenar());
								break;
							}
							case 5:
							{
								switch(parque.tipoVehiculo(vehiculoOpciones)){
									case 1:
									{
										String colorTuneo = JOptionPane.showInputDialog(null, "Introduce un color para tunear: ");
										JOptionPane.showMessageDialog(null, ((Coche)vehiculoOpciones).tunear(colorTuneo));
										break;
									}
									case 2:
									{
										JOptionPane.showMessageDialog(null, ((Autobus)vehiculoOpciones).abrirPuertas());
										break;
									}
									case 3:
									{
										JOptionPane.showMessageDialog(null, ((Motocicleta)vehiculoOpciones).brincar());
										break;
									}
									case 4:
									{
										JOptionPane.showMessageDialog(null, ((Avioneta)vehiculoOpciones).despegar());
										break;
									}
									case 5:
									{
										JOptionPane.showMessageDialog(null, ((Yate)vehiculoOpciones).zarpar());
										break;
									}
									default:
									{
									JOptionPane.showMessageDialog(null, "Introduce un número del 1 al 6");
									break;
									}
								}
							break;
							}

							case 6:
							{
								switch(parque.tipoVehiculo(vehiculoOpciones)){
									case 1:
									{
										JOptionPane.showMessageDialog(null, ((Coche)vehiculoOpciones).aparcar());
										break;
									}
									case 2:
									{
										JOptionPane.showMessageDialog(null, ((Autobus)vehiculoOpciones).aparcar());
										break;
									}
									case 3:
									{
										JOptionPane.showMessageDialog(null, ((Motocicleta)vehiculoOpciones).aparcar());
										break;
									}
									case 4:
									{
										JOptionPane.showMessageDialog(null, ((Avioneta)vehiculoOpciones).aterrizar());
										break;
									}
									case 5:
									{
										JOptionPane.showMessageDialog(null, ((Yate)vehiculoOpciones).atracar());
										break;
									}
									default:
									{
									JOptionPane.showMessageDialog(null, "Introduce un número del 1 al 6");
									break;
									}
								}
							break;
							}

							default:
							{
								JOptionPane.showMessageDialog(null, "Introduce un número del 1 al 6");
								break;
							}
						}

					}while(exitOpciones == false);
					break;
				}

				case 13:
				{
					JOptionPane.showMessageDialog(null, "Gracias por utilizar nuestro parque. Hasta la próxima.");
					exit = true;
					break;
				}

				default:
				{
					JOptionPane.showMessageDialog(null, "Introduce un número del 1 al 13");
					break;
				}
			}


		}while(exit == false);
	}

	

	public int tipoVehiculo(Vehiculo unVehiculo){
		String nomClasse = unVehiculo.getClass().getSimpleName();
		
		switch(nomClasse){
			case "Coche":
				return 1;
			case "Autobus":
				return 2;
			case "Motocicleta":
				return 3;
			case "Avioneta":
				return 4;
			case "Yate":
				return 5;
			default:
				return -1;
		}  
	}


	public void mostrarArray(ArrayList<Vehiculo> miArrayList){
		int numClasse;

		for (Vehiculo v: miArrayList){
			numClasse = tipoVehiculo(v);

			switch(numClasse){
				case 1:{
					JOptionPane.showMessageDialog(null,v + "\n" + ((Coche)v).circular());
					break;
				}
				case 2:{
					JOptionPane.showMessageDialog(null,v + "\n" + ((Autobus)v).circular());
					break;
				}
				case 3:{
					JOptionPane.showMessageDialog(null,v + "\n" + ((Motocicleta)v).circular());
					break;
				}
				case 4:{
					JOptionPane.showMessageDialog(null,v + "\n" + ((Avioneta)v).volar());
					break;
				}
				case 5:{
					JOptionPane.showMessageDialog(null,v + "\n" + ((Yate)v).navegar());
					break;
				}
				default:{
					JOptionPane.showMessageDialog(null,"Error. Tipo de clase incorrecto.");
					break;
				}
			}
		}
	}
}