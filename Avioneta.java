public class Avioneta extends Vehiculo implements PuedeVolar{

	private String aeropuerto;
	private int maxKg;

	/* constructors */

	public Avioneta(){
		super();
		this.aeropuerto = "";
		this.maxKg = 0;
	}

	public Avioneta(String marca, String modelo, String color, double kilometros, int numPuertas, int numPlazas, String aeropuerto, int maxKg){
		super(marca, modelo, color, kilometros, numPuertas, numPlazas);
		this.setAeropuerto(aeropuerto);
		this.setMaxKg(maxKg);
	}

	/* getters */

	public String getAeropuerto(){
		return aeropuerto;
	}

	public int getMaxKg(){
		return maxKg;
	}

	/* setters */

	public void setAeropuerto(String aeropuerto){
		this.aeropuerto = aeropuerto;
	}

	public void setMaxKg(int maxKg){
		this.maxKg = maxKg;
	}

	/* mètodes */

	public String despegar(){
		return "La avioneta está despegando";
	}

	public String aterrizar(){
		return "La avioneta está aterrizando";
	}

	@Override
	public String arrancar(){
		return "La avioneta está arrancando";
	}

	@Override
	public String acelerar(){
		return "La avioneta está acelerando";
	}

	@Override
	public String frenar(){
		return "La avioneta está frenando";
	}

	@Override
	public String volar(){
		return "Esto es una avioneta y las avionetas sólo pueden circular dentro de los aeropuertos.";
	}

	@Override
	public String toString(){
		return super.toString()
				+ "\nAeropuerto: " + getAeropuerto()
				+ "\nPeso máximo: " + getMaxKg();
	}
}